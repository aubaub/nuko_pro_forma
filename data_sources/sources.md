# Sources

## Texas workers compensation opt-out list
## Texas workers comp 3-month zip file
18k companies in Texas. Includes their name, address, NAICS code
https://www.tdi.texas.gov/wc/employer/coverage.html

## WC data for many states:  https://www.ewccv.com/cvs
34 states + DC

## Find the websites for the missing states here:
https://www.workcompconsultant.com/v3-verify-workers-comp-states-f-g/
I haven't looked to see how many of the others are present

# Method for getting data
The national website requires you to give search terms, limiting results per search.
It requires a captcha at some unknown frequency, but lets you see all the results
from your search before having to complete another one.

To maximize the value of search results, first go to state SOS's to get lists
of all the companies in a state.
We can filter by industry keywords or something, then find the most popular
words & search the national WC site for those words in those states.

Results only get returned for 1 result date. To find effective dates,
we'll have to keep track of the policy # on each result date, then try
different dates to find when the policy number changes. Maybe start
with 4 searches/year to get the quarter that the policy renews,
then when we're in that quarter & people need to make calls, then
go month/month to prioritize calls.

# Order of work
1. Get one state's SOS data. Use it as the basis for our CRM.
2. Do some made-up math to make up customer probability rankings
    - deduct points from companies that are prime standard lines customers
    - add points for words like doctor, lawyer, condominium, etc
    - figure out how to estimate company size
    - check out the work I did on corporate life expectancy for ideas
3. Using the CRM, search the national site until you find the month
    that their policy number changes. Save each search result in the CRM.
4. Build a crawler to gather data for the CRM
    - Does the company have LinkedIn/Facebook/etc? How many employees/followers
    - How many web hits seem relevant to them?
    - Is their website one of the top Google results? Can we find their website?
    - What happens when you Google their name + address? Results?
    - How many pages does their website have when you follow all of its links?

# What's done
1. Florida SOS data: desktop/state data/Florida
2. 
3. Records can be easily scraped from https://ProofOfCoverage.fldfs.com
    Has effective dates, carrier name, policy number, NAICS code, # of employees,
    "Coverage class code", which is like NAICS codes.  https://www.insurancexdate.com/class/FL
    CCC is beautifully specific. "Billiard--hall" "Bowling lane" "Potato chip, popcorn & Snack Chip mfg NOC"
4. 
