from flask import Flask, render_template, send_file
import base64
import profit_and_loss as p_l

app = Flask(__name__)

@app.route('/')
def hello_world():
	plot_pic = p_l.do_plot()
	
	# plot_pic = p_l.pert_distribution()
	# return plot_pic
	# plot_encoded = base64.b64encode(plot_pic.
	# return render_template('test.html', plot_url=plot_pic)

	return send_file(plot_pic, attachment_filename="pic.png", mimetype='image/png')

