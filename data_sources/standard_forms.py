""" Acts like a human, in a human-style workflow, to collect data from the NAIC """
import argparse
import sys
import time
import math
import pickle
import datetime
import os
from pathlib import Path
import random

from selenium import webdriver
from bs4 import BeautifulSoup
from bs4.element import NavigableString
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile

BASE_URL = "https://filingaccess.serff.com/sfa/home/"
POLICIES_OF_INTEREST = [
    'Property', 'Flood', 'CMP Liability and Non-Liability',
    'CMP Non-Liability Portion Only', 'CMP Liability Portion Only',
    'Earthquake', 'Other Liability-Occ/Claims Made',
    'Other Liability-Occ Only', 'Other Liability-Occurrence Only',
    'Other Liability-Claims Made Only', 'Product Liability',
    'Commercial Auto', 'Burglary and Theft', 'Boiler and Machinery',
    'Boiler and Machinery or Equipment Breakdown',
    'Other Lines of Business', 'Miscellaneous']
LOWERCASE_POLICIES_OF_INTEREST = [pol.lower() for pol in POLICIES_OF_INTEREST]


def grab_data():
    """ For a given state and time period, get data and download forms for all
    filings with rulings (accepted, rejected, etc) made during the time period.
    Pickle the data from the website. """
    args = process_arguments()
    driver = setup_driver(args)
    fill_in_search_page(driver, args)
    collected_information = iterate_through_search_results(driver)
    filename_dates = str(args.start_date.replace("/", "-")) + "__" \
        + str(args.end_date.replace("/", "-"))
    filename_for_data = str(args.state) + "__" + filename_dates + '.p'
    with open('pickles/' + filename_for_data, 'wb') as handler:
        pickle.dump(collected_information, handler,
            protocol=pickle.HIGHEST_PROTOCOL)
    driver.quit()
 # Alert the user that we're done processing by being very annoying
    for unused_var in range(30): # pylint: disable=unused-variable
        time.sleep(.5)
        print('\a') # Makes a ding


def setup_driver(args):
    """ Either sets up Tor or Chrome to drive viewing the web pages """
    if args.driver.lower() == 'chrome':
        driver = webdriver.Chrome()
    else: 
        profile = FirefoxProfile()
        profile.set_preference('network.proxy.socks_port', 9150)
        profile.set_preference('network.proxy.socks', '127.0.0.1')
        # Don't download images to speed things up
        profile.set_preference('permissions.default.image', 2)
        profile.set_preference('privacy.sanitize.sanitizeOnShutdown', True)
        profile.set_preference('places.history.enabled', False)
        profile.set_preference('network.proxy.type', 1)
        profile.update_preferences()
        driver = webdriver.Firefox(firefox_profile=profile)

    driver.implicitly_wait(30)
    return driver


def fill_in_search_page(driver, args):
    """ Inputs values to limit your search results """
    # Go to the search page 
    driver.get(BASE_URL + str(args.state))
    driver.find_element_by_link_text("Begin Search").click()
    # Accept the terms & conditions 
    driver.find_element_by_id("j_idt18:j_idt20").click()
    # Fill in search requests
    # Select the business type (Property & Casualty)
    driver.find_element_by_id("simpleSearch:businessType").click()
    driver.find_element_by_id("simpleSearch:businessType_panel").\
        find_elements_by_tag_name("li")[1].click()
    # Select the types of insurance to filter with
    driver.find_element_by_id("simpleSearch:availableTois").click()
    policy_options = driver.find_element_by_class_name(
        "ui-selectcheckboxmenu-items-wrapper").\
        find_elements_by_tag_name("li")
    # Select the policy types you want information on. 
    # TODO: it would be better to select all policies, then exclude commonly
    # listed policies you don't want. The list of policies can change from 
    # state to state, and we'll miss some useful ones this way. 
    web_policy_options = {element.text.lower(
        ): element for element in policy_options}
    for web_option in web_policy_options:
        if any(interest in web_option for interest in \
            LOWERCASE_POLICIES_OF_INTEREST):
            web_policy_options[web_option].click()
    # Fill in the search dates
    driver.find_element_by_name("simpleSearch:dispositionStartDate_input").\
        send_keys(args.start_date)
    driver.find_element_by_name("simpleSearch:dispositionEndDate_input").\
        send_keys(args.end_date)
    # Submit
    driver.find_element_by_name("simpleSearch:saveBtn").click()


def iterate_through_search_results(driver):
    """ Click through to each filing returned in the search results """
    collected_information = {}
    # Change 'Show" from 20 per page to 100 per page 
    paginator_options = driver.find_element_by_id("j_idt25:filingTable_rppDD")
    paginator_options.click()
    paginator_options.find_elements_by_tag_name("option")[-1].click()
    number_of_records = driver.find_element_by_xpath(
        '//*[@id="bodyContentWrapper"]/div[2]/span/span')
    number_of_records = int(number_of_records.text.replace(",", ''))
    pages_of_records = math.ceil(number_of_records / 100)
    # The average amount of time it takes to get a record is ~6 seconds 
    time_to_complete = number_of_records * 6
    print("Records being searched:", number_of_records)
    print("Start time:", datetime.datetime.now().strftime('%H:%M'))
    print("Estimated finish time:",
        (datetime.datetime.now() + datetime.timedelta(seconds=time_to_complete)).strftime('%H:%M'))
    result_page = 0
    while result_page < pages_of_records:
        table_of_filings = driver.find_element_by_id( "j_idt25:filingTable_data")
        time.sleep(.5)
        for table_row in range(len(table_of_filings.find_elements_by_tag_name(
            'tr'))):

            # Leaving & returning to the index page causes an error,
            # StaleElementReferenceException, when trying to continue to iterate
            # over the rows. You have to reload table_of_filings each time. 
            try: 
                time.sleep(abs(random.gauss(1.5, .5)) + .5)
                table_of_filings = driver.find_element_by_id(
                    "j_idt25:filingTable_data")
                filing_row = table_of_filings.find_elements_by_tag_name('tr')[
                    table_row]
                filing_row.click()

                soup = BeautifulSoup(
                    driver.find_element_by_id("j_idt28_content").
                    get_attribute('innerHTML'), features="html.parser")

                data_from_soup = save_table_data_to_dict(soup)
                collected_information[data_from_soup['filing_summary'][
                    'SERFF Tracking Number:']] = data_from_soup

                # Download the zip file, after a human-like pause
                time.sleep(abs(random.gauss(2, .5)))
                driver.find_element_by_id("summaryForm:downloadLink").click()

                # Go back to the search results
                driver.back()
            except IndexError:
                print("There was an IndexError when trying to find the row to click. ")

        # Click the pagination arrow to get the next page of results 
        # The first instance of the 'next' arrow is at the top of the page:
        driver.execute_script("window.scrollTo(0, 0);")
        time.sleep(.5)
        driver.find_element_by_class_name("ui-icon-seek-next").click()
        result_page += 1

    return collected_information


def save_table_data_to_dict(soup):
    """ Aggregate all page data into one dictionary """
    collected_data = {}
    collected_data['filing_summary'] = save_filing_information_and_outcome(
        soup)
    collected_data['company_info'] = save_company_information(soup)
    collected_data['attachment_data'] = save_attachments_data(soup)
    return collected_data


def save_filing_information_and_outcome(soup): 
    """ The Filing Information an Filing Outcome sections have similar structures.
        Grab all the information in those sections. """
    collected_vars = {}
    expected_headers = ["Filing Information", "Filing Outcome"]
    for header in expected_headers:
        filing_info_parent = soup.find(text=header).parent.parent
        for detail in filing_info_parent.findAll('div'):
            var_name = detail.label 
            if var_name is not None and not isinstance(
                var_name.next_sibling, NavigableString):
                collected_vars[var_name.text.strip()] = var_name.next_sibling.\
                    text.strip()
    return collected_vars


def save_company_information(soup):
    """ Grabs information listed about the carrier making the filing request """
    company_info = soup.find(text="Company Information").parent
    # The first sibling coantains headers, with data in the second sibling
    header_parent = company_info.next_sibling.next_sibling
    company_headers = []
    for item in header_parent.contents:
        if item is not None and not isinstance(item, NavigableString):
            company_headers += [item.text.strip()]
    company_data = []
    data_parent = header_parent.next_sibling.next_sibling
    for item in data_parent.contents:
        if item is not None and not isinstance(item, NavigableString):
            company_data += [item.text.strip().replace('\n', '').
                replace('\t', '')]
    return dict(zip(company_headers, company_data))


def save_attachments_data(soup): # pylint: disable=too-many-locals 
    """ There can be dozens of attachments contained in the zip files.
        Capture all of the information about those attachments. """
    returned_dictionary = {}
    attachments_container = soup.find(id="attachmentsContainer")
    for panel in attachments_container.findAll('div', recursive=False): 
        if isinstance(panel, NavigableString):
            continue
        column_headers = []
        column_data = []
        panel_title = panel.div.span.text.strip()
        # The 2nd div has the good stuff
        divs_there = panel.contents[1]
        another_kid = divs_there.find('div')
        # Last div before divs with data
        if another_kid is None:
            continue
        more_kids = another_kid.contents
        # First level with real data
    
        for div in more_kids[1]:
            try:
                column_headers += [div.text.strip()]
            except (TypeError, AttributeError):
                pass

        # Even-numbered rows are blank lines. Odds have real info
        for row_with_data in more_kids[2:]:
            try:
                if len(row_with_data.findAll("div")) > 1:
                    column_key = row_with_data.findAll("div")[0].text.strip()
                    starting_values = [row.text.strip() for row in
                        row_with_data.findAll("div")[1:]]
    
                    # When there are multiple items in a div, you get lots of
                    #  weird line breaks. Remove them.
                    cleaned_values = []
                    for stuff in starting_values: 
                        if "\n" in stuff:
                            cleaned_values += list(filter(None, stuff.split( "\n")))
                        else:
                            cleaned_values += [stuff]
    
                    # There are duplicates in the column_values. Remove them.
                    column_values = list(set(cleaned_values))
                    column_data += [{column_key: column_values}]

            except AttributeError: # happens when there's no info present
                pass

        returned_dictionary[panel_title] = [column_headers, column_data]

    return returned_dictionary


def process_arguments():
    """ Let users pass in 3 args through bash: state, start_date, and end_date """
    parser = argparse.ArgumentParser(
        description="Utility for helping access NAIC records", add_help=False )

    parser.add_argument(
        "--start_date",
        help="The earliest disposition date to search in M/D/YY format",
        dest="start_date", metavar="<start_date>")

    parser.add_argument(
        "--end_date",
        help="The ending disposition date of your search in M/D/YY format",
        dest="end_date", metavar="<start_date>")

    parser.add_argument(
        "--state",
        help="The state to request filings from",
        dest="state", metavar="<state>", )

    parser.add_argument( "--driver", help="Which driver to use, Chrome or Tor",
        dest="driver", metavar="<driver>", default="chrome" )

    if len(sys.argv[1:]) == 0:
        parser.print_help()
    
    return parser.parse_args()


if __name__ == "__main__":
    grab_data()