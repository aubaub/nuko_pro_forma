import time
import random

from selenium import webdriver
from bs4 import BeautifulSoup
from bs4.element import NavigableString
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile

BASE_URL = "https://proofofcoverage.fldfs.com/Search"


def get_data():
    driver = setup_driver()
    fill_in_search_page(driver, "bike")
    records = return_records(driver)
    # save_records_to_database(records)


def setup_driver(tor=True):
    """ Either sets up Tor or Chrome to drive viewing the web pages """
    if tor:
        profile = FirefoxProfile()
        profile.set_preference('network.proxy.socks_port', 9150)
        profile.set_preference('network.proxy.socks', '127.0.0.1')
        # Don't download images to speed things up
        profile.set_preference('permissions.default.image', 2)
        profile.set_preference('privacy.sanitize.sanitizeOnShutdown', True)
        profile.set_preference('places.history.enabled', False)
        profile.set_preference('network.proxy.type', 1)
        profile.update_preferences()
        driver = webdriver.Firefox(firefox_profile=profile)
    else:
        driver = webdriver.Chrome()

    driver.implicitly_wait(30)
    return driver


def fill_in_search_page(driver, search_term):
    """ Search the database for all results of search_term """
    driver.get(BASE_URL)

    employer_name_input = driver.find_element_by_id(
        "MainContent_txt01").send_keys(search_term)
    # You can request up to 999,999,999 records/search
    max_records_input = driver.find_element_by_id(
        "MainContent_txt03").send_keys(
        int(round(random.uniform(20000, 100000), -3)))
    driver.find_element_by_id("MainContent_btnSearch").click()


def return_records(driver):
    """ Save all useful data to a dictionary """
    records = {}

    table = driver.find_element_by_id("MainContent_Table1")

    # The 4th row in the table body is the 1st row with data
    for row_index, row in enumerate(table_of_filings.find_elements_by_tag_name(
        'tr')):
        # Grab all hrefs with employerIDs
        # filing_row = table_row.find_elements_by_tag_name('tr')[row_index]
        columns = row.find_elements_by_tag_name('td')
        # Column 1 is useless
        # Column 2 is employer name
        employer_name = columns[1].text()
        records[employer_name] = {}
        # Column 3 is employer address
        # Column 4 is coverage
        # Column 5 is exemption
        # Column 6 is Elec (?)

        pass

    #### TODO: Check the employer ID in the DB to see
    #### if we already have this record


    # Coverage, Exemption, & Elec (?)