# Making a dynamic P&L using monte carlo simulations to draw
# from beta pert distributions

from PertDist.pert import PERT
import seaborn as sns
import io
# import StringIO
# import matplotlib.pyplot as plt
import base64

from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanva



import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import io
from sklearn.datasets import load_breast_cancer
def do_plot():
    # Loading 
    # data = load_breast_cancer()
    # breast_cancer_df = pd.DataFrame(data['data'])
    # breast_cancer_df.columns = data['feature_names']
    # breast_cancer_df['target'] = data['target']
    # breast_cancer_df['diagnosis'] = [data['target_names'][x] for x in data['target']]
    # feature_names= data['feature_names']

    # corr = breast_cancer_df[list(feature_names)].corr(method='pearson')

    # f, ax = plt.subplots(figsize=(11, 9))
    # cmap = sns.diverging_palette(220, 10, as_cmap=True)
    # mask = np.zeros_like(corr, dtype=np.bool)
    # mask[np.triu_indices_from(mask)] = True

    # sns.heatmap(corr, mask=mask, cmap=cmap, vmax=.3, center=0,
                # square=True, linewidths=.5, cbar_kws={"shrink": .5})
				
	y = [1,2,3,4,5]
	x = [0,2,1,3,4]
	plt.plot(x, y)
	plt.savefig(img, format='png')
	plt.close()
	img.seek(0)
	
	plot_url = base64.b64encode(img.getvalue())

	# here is the trick save your figure into a bytes object and you can #afterwards expose it via flas
	bytes_image = io.BytesIO()
	plt.savefig(bytes_image, format='png')
	bytes_image.seek(0)
	return bytes_image





IMPORTANT_TEMRS = {
	'avg_lead_value': {4, 6.4, 10},
	'quotes_per_account': {2, 4, 6},
	'avg_apr_over_prime': {.5, 1.5, 2.5},
	'cof_under_prime': {0, 1, 2},
	'write_offs': {.05, .2, .3},
	'fee_ratio': {.03, .057, .065},
	}

def pert_distribution():
	pert = PERT(10, 190, 200)
	pert_draw = pert.rvs(100)
	print("DATA:", pert_draw)
	# plot_pic = sns.kdeplot(pert.rvs(100)).figure

	img = io.BytesIO()
	sns.set_style("dark")
	
	y = [1,2,3,4,5]
	x = [0,2,1,3,4]
	
	plt.plot(x, y)
	plt.savefig(img, format='png')
	plt.close()
	img.seek(0)
	
	plot_url = base64.b64encode(img.getvalue())
	# bytes_image = io.BytesIO()
	# plot_pic
	return plot_url

	
	
	
# if __name__ == '__main__':
	# pert = PERT(10, 190, 200)
	# plot_pic = sns.kdeplot(pert.rvs(10000)).figure.savefig("example.png")
